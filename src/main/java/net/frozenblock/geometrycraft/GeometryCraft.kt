package net.frozenblock.geometrycraft

import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking
import net.fabricmc.loader.api.FabricLoader
import net.frozenblock.geometrycraft.command.GDCommand
import net.frozenblock.geometrycraft.command.GDModeArgument
import net.frozenblock.geometrycraft.datafix.GDDataFixer
import net.frozenblock.geometrycraft.entity.Checkpoint
import net.frozenblock.geometrycraft.entity.Orb
import net.frozenblock.geometrycraft.entity.Portal
import net.frozenblock.geometrycraft.entity.data.GCEntityData
import net.frozenblock.geometrycraft.entity.data.GCPlayerData
import net.frozenblock.geometrycraft.network.C2SExitPacket
import net.frozenblock.geometrycraft.network.C2SFailPacket
import net.frozenblock.geometrycraft.network.GCNetworking
import net.frozenblock.geometrycraft.portal.RegisterPortal
import net.frozenblock.geometrycraft.registry.*
import net.frozenblock.geometrycraft.util.*
import net.frozenblock.geometrycraft.worldgen.SchematicChunkGenerator
import net.frozenblock.lib.gravity.api.GravityAPI
import net.minecraft.commands.synchronization.ArgumentTypeInfos
import net.minecraft.commands.synchronization.SingletonArgumentInfo
import net.minecraft.core.registries.BuiltInRegistries
import net.minecraft.core.registries.Registries
import net.minecraft.network.chat.Component
import net.minecraft.network.syncher.EntityDataSerializers
import net.minecraft.resources.ResourceKey
import net.minecraft.world.damagesource.DamageType
import net.minecraft.world.item.CreativeModeTab
import net.minecraft.world.item.ItemStack
import net.minecraft.world.item.Items
import net.minecraft.world.level.Level
import net.minecraft.world.level.dimension.DimensionType
import kotlin.system.measureNanoTime

object GeometryCraft : ModInitializer {

    @JvmField
    val CREATIVE_TAB: ResourceKey<CreativeModeTab> = register(
        "creative_tab",
        FabricItemGroup.builder()
            .title(Component.literal("Geometry Craft"))
            .icon { ItemStack(Items.DIAMOND) }
            .displayItems { params, entries ->
                entries.accept(RegisterBlocks.SPIKE)
                entries.accept(RegisterBlocks.LOW_JUMP_PAD)
                entries.accept(RegisterBlocks.JUMP_PAD)
                entries.accept(RegisterBlocks.HIGH_JUMP_PAD)
                entries.accept(RegisterBlocks.REVERSE_GRAVITY_JUMP_PAD)
                entries.accept(RegisterBlocks.TELEPORT_PAD)
                entries.accept(RegisterBlocks.RAINBOW_BLOCK)
                entries.accept(RegisterBlocks.RED_BLOCK)
                entries.accept(RegisterBlocks.ORANGE_BLOCK)
                entries.accept(RegisterBlocks.YELLOW_BLOCK)
                entries.accept(RegisterBlocks.LIME_BLOCK)
                entries.accept(RegisterBlocks.LIGHT_BLUE_BLOCK)
                entries.accept(RegisterBlocks.BLUE_BLOCK)

                entries.accept(RegisterItems.EDIT_TOOL)
            }
            .build()
    ).key!!

    @JvmField
    val OLD_DIMENSION = oldId("geometry")

    @JvmField
    val MID_DIMENSION = midId("the_other_side")

    @JvmField
    val SPECIAL_EFFECTS = id("the_other_side")

    @JvmField
    val DIMENSION_TYPE: ResourceKey<DimensionType> = ResourceKey.create(Registries.DIMENSION_TYPE, id("the_other_side"))

    @JvmField
    val DIMENSION: ResourceKey<Level> = ResourceKey.create(Registries.DIMENSION, id("the_other_side"))

    @JvmField
    val LEVEL_FAIL: ResourceKey<DamageType> = ResourceKey.create(Registries.DAMAGE_TYPE, id("level_fail"))

    override fun onInitialize() {
        val time = measureNanoTime {
            GDRegistries
            GDDataFixer.applyDataFixes(FabricLoader.getInstance().getModContainer(MOD_ID).orElseThrow())

            RegisterBlocks
            RegisterBlockEntities
            RegisterEntities
            RegisterItems
            RegisterScaleTypes
            RegisterSounds

            BuiltInRegistries.CHUNK_GENERATOR.register("schematic", SchematicChunkGenerator.CODEC)

            RegisterPortal.init()

            ArgumentTypeInfos.register(
                BuiltInRegistries.COMMAND_ARGUMENT_TYPE,
                string("gd_mode"),
                GDModeArgument::class.java,
                SingletonArgumentInfo.contextFree(GDModeArgument::gdMode)
            )

            CommandRegistrationCallback.EVENT.register { dispatcher, _, _ ->
                GDCommand.register(dispatcher)
            }

            GCNetworking.registerC2SPayloadTypes()

            ServerPlayNetworking.registerGlobalReceiver(C2SFailPacket.TYPE) { packet, ctx ->
                val player = ctx.player()
                player.hurt(player.damageSources().source(LEVEL_FAIL), Float.MAX_VALUE)
            }
            ServerPlayNetworking.registerGlobalReceiver(C2SExitPacket.TYPE) { packet, ctx ->
                ctx.player().gdData.exitGD()
            }

            GCEntityData
            GCPlayerData
            EntityDataSerializers.registerSerializer(Checkpoint.CheckpointType.SERIALIZER)
            EntityDataSerializers.registerSerializer(Orb.OrbType.SERIALIZER)
            EntityDataSerializers.registerSerializer(Portal.PortalType.SERIALIZER)

            GravityAPI.MODIFICATIONS.register { ctx ->
                val entity = ctx.entity ?: return@register
                val gravity = entity.gdGravity
                ctx.gravity = gravity
            }
        }

        log("Geometry Craft took $time nanoseconds")
    }
}
