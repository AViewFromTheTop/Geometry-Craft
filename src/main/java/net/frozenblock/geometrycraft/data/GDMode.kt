package net.frozenblock.geometrycraft.data

import com.mojang.serialization.Codec
import net.frozenblock.geometrycraft.data.mode.*
import net.frozenblock.geometrycraft.data.mode.CubeModeData
import net.minecraft.util.StringRepresentable
import org.jetbrains.annotations.Contract

enum class GDMode(val title: String, val isFlying: Boolean, val modeData: () -> GDModeData) : StringRepresentable {
    CUBE("Cube", false, { CubeModeData() }),
    SHIP("Ship", true, { ShipModeData() }),
    BALL("Ball", false, { BallModeData() }),
    UFO("UFO", true,  { UFOModeData() }),
    WAVE("Wave", true, { WaveModeData() }),
    ROBOT("Robot", false, { RobotModeData() }),
    SPIDER("Spider", false, { SpiderModeData() }),
    SWING("Swing", true, { SwingModeData() }),

    CUBE_3D("Cube 3D", false, { Cube3DModeData() }),
    ROBOT_3D("Robot 3D", false, { Robot3DModeData() });

    companion object {
        @JvmField
        val CODEC: Codec<GDMode> = StringRepresentable.fromEnum(::values)
    }

    @Contract(pure = true)
    override fun getSerializedName(): String = this.name.lowercase()
}
