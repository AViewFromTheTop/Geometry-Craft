package net.frozenblock.geometrycraft.duck

import de.keksuccino.melody.resources.audio.openal.ALAudioClip

interface MCDuck {
    var `geometryDash$audioClip`: ALAudioClip?
}
