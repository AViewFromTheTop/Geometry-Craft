package net.frozenblock.geometrycraft.entity.render

import net.frozenblock.geometrycraft.entity.Portal
import net.frozenblock.geometrycraft.util.id
import net.minecraft.client.renderer.RenderType
import net.minecraft.client.renderer.entity.EntityRendererProvider.Context
import net.minecraft.resources.ResourceLocation

open class PortalRenderer(ctx: Context) : StaticEntityRenderer<Portal>(ctx) {

    companion object {
        private val CUBE_TEXTURE = id("textures/entity/cube_portal.png")
        private val CUBE_LAYER = RenderType.entityTranslucentEmissive(CUBE_TEXTURE)

        private val SHIP_TEXTURE = id("textures/entity/ship_portal.png")
        private val SHIP_LAYER = RenderType.entityTranslucentEmissive(SHIP_TEXTURE)

        private val BALL_TEXTURE = id("textures/entity/ball_portal.png")
        private val BALL_LAYER = RenderType.entityTranslucentEmissive(BALL_TEXTURE)

        private val UFO_TEXTURE = id("textures/entity/ufo_portal.png")
        private val UFO_LAYER = RenderType.entityTranslucentEmissive(UFO_TEXTURE)

        private val WAVE_TEXTURE = id("textures/entity/wave_portal.png")
        private val WAVE_LAYER = RenderType.entityTranslucentEmissive(WAVE_TEXTURE)

        private val ROBOT_TEXTURE = id("textures/entity/robot_portal.png")
        private val ROBOT_LAYER = RenderType.entityTranslucentEmissive(ROBOT_TEXTURE)

        private val SPIDER_TEXTURE = id("textures/entity/spider_portal.png")
        private val SPIDER_LAYER = RenderType.entityTranslucentEmissive(SPIDER_TEXTURE)

        private val SWING_TEXTURE = id("textures/entity/swing_portal.png")
        private val SWING_LAYER = RenderType.entityTranslucentEmissive(SWING_TEXTURE)
    }

    override fun scale(entity: Portal): Float = 1F
    override fun width(entity: Portal): Float = 1.85F
    override fun height(entity: Portal): Float = 2F
    override fun yOffset(entity: Portal): Float = 1F

    override fun getTextureLocation(entity: Portal): ResourceLocation {
        return when (entity.type) {
            Portal.PortalType.CUBE -> CUBE_TEXTURE
            Portal.PortalType.SHIP -> SHIP_TEXTURE
            Portal.PortalType.BALL -> BALL_TEXTURE
            Portal.PortalType.UFO -> UFO_TEXTURE
            Portal.PortalType.WAVE -> WAVE_TEXTURE
            Portal.PortalType.ROBOT -> ROBOT_TEXTURE
            Portal.PortalType.SPIDER -> SPIDER_TEXTURE
            Portal.PortalType.SWING -> SWING_TEXTURE

            Portal.PortalType.CUBE_3D -> CUBE_TEXTURE
            Portal.PortalType.ROBOT_3D -> ROBOT_TEXTURE
            else -> CUBE_TEXTURE
        }
    }

    override fun getLayer(entity: Portal): RenderType {
        return when (entity.type) {
            Portal.PortalType.CUBE -> CUBE_LAYER
            Portal.PortalType.SHIP -> SHIP_LAYER
            Portal.PortalType.BALL -> BALL_LAYER
            Portal.PortalType.UFO -> UFO_LAYER
            Portal.PortalType.WAVE -> WAVE_LAYER
            Portal.PortalType.ROBOT -> ROBOT_LAYER
            Portal.PortalType.SPIDER -> SPIDER_LAYER
            Portal.PortalType.SWING -> SWING_LAYER

            Portal.PortalType.CUBE_3D -> CUBE_LAYER
            Portal.PortalType.ROBOT_3D -> ROBOT_LAYER
            else -> CUBE_LAYER
        }
    }
}
