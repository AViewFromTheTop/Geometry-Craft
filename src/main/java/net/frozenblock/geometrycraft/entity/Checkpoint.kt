@file:Suppress("DEPRECATION")

package net.frozenblock.geometrycraft.entity

import io.netty.buffer.ByteBuf
import net.frozenblock.geometrycraft.data.CheckpointSnapshot
import net.frozenblock.geometrycraft.data.GDData
import net.frozenblock.geometrycraft.data.SongSource
import net.frozenblock.geometrycraft.data.SongSource.Companion.getSongSource
import net.frozenblock.geometrycraft.data.SongSource.Companion.putSongSource
import net.frozenblock.geometrycraft.util.gdData
import net.frozenblock.geometrycraft.util.gravityDirection
import net.frozenblock.geometrycraft.util.gravityStrength
import net.minecraft.nbt.CompoundTag
import net.minecraft.network.codec.ByteBufCodecs
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.syncher.EntityDataAccessor
import net.minecraft.network.syncher.EntityDataSerializer
import net.minecraft.network.syncher.SynchedEntityData
import net.minecraft.server.level.ServerPlayer
import net.minecraft.util.ByIdMap
import net.minecraft.util.StringRepresentable
import net.minecraft.util.StringRepresentable.EnumCodec
import net.minecraft.world.entity.EntityType
import net.minecraft.world.entity.player.Player
import net.minecraft.world.level.Level
import java.util.function.IntFunction

open class Checkpoint(
    type: EntityType<out Checkpoint>,
    level: Level,
) : StaticEntity(type, level) {

    companion object {
        @PublishedApi
        @JvmField
        internal val CHECKPOINT_TYPE: EntityDataAccessor<CheckpointType> = SynchedEntityData.defineId(Checkpoint::class.java, CheckpointType.SERIALIZER)
    }

    inline var type: CheckpointType
        get() = this.`access$entityData`[CHECKPOINT_TYPE]
        set(value) { this.`access$entityData`[CHECKPOINT_TYPE] = value }

    var song: SongSource? = null

    override fun defineSynchedData(builder: SynchedEntityData.Builder) {
        builder.define(CHECKPOINT_TYPE, CheckpointType.STANDARD)
    }

    override fun tick() {
        if (!this.level().isClientSide)
            this.checkpointTick()
    }

    protected open fun addCheckpoint(player: Player, gdData: GDData) {
        val list = gdData.checkpoints
        if (list.map { it.entityId }.contains(this.id)) return
        list.add(CheckpointSnapshot(
            this.id,
            gdData.modeData,
            player.deltaMovement,
            player.xRot,
            player.yRot,
            gdData.scale,
            player.gravityStrength,
            player.gravityDirection,
            player.onGround(),
            gdData.cameraData,
            gdData.isVisible,
            gdData.timeMod,
            gdData.dashOrbID,
            gdData.cameraMirrorProgress,
            gdData.cameraMirrorDirection,
        ))
    }

    protected open fun checkpointTick() {
        val list: List<ServerPlayer> = this.level().getEntitiesOfClass(ServerPlayer::class.java, this.boundingBox)
        for (player in list) {
            if (player.isDeadOrDying) continue
            val gdData = player.gdData
            when (this.type) {
                CheckpointType.START -> {
                    gdData.enterGD(song = this.song)
                    player.xRot = this.xRot
                    player.yRot = this.yRot
                }
                CheckpointType.END -> gdData.exitGD()
                else -> {}
            }
            if (this.type.shouldAddSpawn)
                this.addCheckpoint(player, gdData)
            gdData.markDirty()
        }
    }

    override fun addAdditionalSaveData(compound: CompoundTag) {
        compound.putString("type", this.type.serializedName)
        if (this.type == CheckpointType.START)
            compound.putSongSource("song", this.song)
    }

    override fun readAdditionalSaveData(compound: CompoundTag) {
        this.type = CheckpointType.CODEC.byName(compound.getString("type")) ?: CheckpointType.STANDARD
        if (this.type == CheckpointType.START)
            this.song = compound.getSongSource("song")
    }

    enum class CheckpointType(val shouldAddSpawn: Boolean = true) : StringRepresentable {
        STANDARD,
        START,
        END(false);

        companion object {
            @JvmField
            val CODEC: EnumCodec<CheckpointType> = StringRepresentable.fromEnum(::values)

            @JvmField
            val BY_ID: IntFunction<CheckpointType> = ByIdMap.continuous(CheckpointType::ordinal, entries.toTypedArray(), ByIdMap.OutOfBoundsStrategy.ZERO)

            @JvmField
            val STREAM_CODEC: StreamCodec<ByteBuf, CheckpointType> = ByteBufCodecs.idMapper(BY_ID, CheckpointType::ordinal)

            @JvmField
            val SERIALIZER: EntityDataSerializer<CheckpointType> = EntityDataSerializer.forValueType(STREAM_CODEC)
        }

        override fun getSerializedName(): String = this.name.lowercase()
    }

    @Suppress("unused", "PropertyName")
    @PublishedApi
    internal val `access$entityData`: SynchedEntityData
        get() = entityData
}
