package net.frozenblock.geometrycraft.entity.data

import net.minecraft.nbt.CompoundTag
import net.minecraft.network.syncher.EntityDataAccessor
import net.minecraft.network.syncher.EntityDataSerializers
import net.minecraft.network.syncher.SynchedEntityData
import net.minecraft.world.entity.player.Player

object GCPlayerData {

    @JvmField
    val GD_DATA: EntityDataAccessor<CompoundTag> = SynchedEntityData.defineId(
        Player::class.java, EntityDataSerializers.COMPOUND_TAG
    )
}
