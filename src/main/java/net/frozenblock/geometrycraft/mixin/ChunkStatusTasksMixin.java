package net.frozenblock.geometrycraft.mixin;

import net.frozenblock.geometrycraft.worldgen.SchematicChunkGenerator;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.status.ChunkStatus;
import net.minecraft.world.level.chunk.status.ChunkStatusTasks;
import net.minecraft.world.level.chunk.status.ToFullChunk;
import net.minecraft.world.level.chunk.status.WorldGenContext;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Mixin(ChunkStatusTasks.class)
public class ChunkStatusTasksMixin {

	@Inject(method = "generateNoise", at = @At("HEAD"))
	private static void setSchematicLevel(WorldGenContext context, ChunkStatus status, Executor executor, ToFullChunk toFullChunk, List<ChunkAccess> cache, ChunkAccess loadingChunk, CallbackInfoReturnable<CompletableFuture<ChunkAccess>> cir) {
		if (context.generator() instanceof SchematicChunkGenerator schem) {
			schem.init(context.level());
		}
	}
}
