package net.frozenblock.geometrycraft.mixin.client;

import com.llamalad7.mixinextras.sugar.Local;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.frozenblock.geometrycraft.data.GDData;
import net.frozenblock.geometrycraft.duck.PlayerDuck;
import net.frozenblock.geometrycraft.network.C2SExitPacket;
import net.frozenblock.geometrycraft.util.GDUtilsKt;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.SpriteIconButton;
import net.minecraft.client.gui.layouts.GridLayout;
import net.minecraft.client.gui.screens.PauseScreen;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PauseScreen.class)
public abstract class PauseScreenMixin extends Screen {

	protected PauseScreenMixin(Component title) {
		super(title);
	}

	@SuppressWarnings("NoTranslation")
	@Unique
	private static final Component EXIT_GD = Component.translatable("menu.geometry_craft.exit_gd");

	@SuppressWarnings({"DataFlowIssue", "SpellCheckingInspection"})
	@Inject(method = "createPauseMenu", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/layouts/GridLayout;arrangeElements()V"))
	private void addGDButton(CallbackInfo ci, @Local GridLayout.RowHelper rowHelper) {
		if (this.minecraft.player == null)
			return;
		GDData data = ((PlayerDuck) this.minecraft.player).geometryDash$getGDData();
		if (data.getPlayingGD()) {
			rowHelper.addChild(SpriteIconButton.builder(EXIT_GD, widget -> {
				widget.active = false;
				ClientPlayNetworking.send(new C2SExitPacket());
				this.minecraft.setScreen(null);
			}, true).width(20).sprite(GDUtilsKt.id("icon/exit"), 15, 15).build());
		}
	}
}
