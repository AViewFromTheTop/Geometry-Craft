package net.frozenblock.geometrycraft.registry

import net.frozenblock.geometrycraft.block.entity.JumpPadBlockEntity
import net.minecraft.world.level.block.entity.BlockEntityType

object RegisterBlockEntities {

    @JvmField
    val JUMP_PAD: BlockEntityType<JumpPadBlockEntity> = register(
        "jump_pad",
        ::JumpPadBlockEntity,
        RegisterBlocks.LOW_JUMP_PAD,
        RegisterBlocks.JUMP_PAD,
        RegisterBlocks.HIGH_JUMP_PAD,
        RegisterBlocks.REVERSE_GRAVITY_JUMP_PAD,
        RegisterBlocks.TELEPORT_PAD
    )
}
