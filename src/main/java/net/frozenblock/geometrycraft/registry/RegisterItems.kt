package net.frozenblock.geometrycraft.registry

import net.frozenblock.geometrycraft.item.EditTool
import net.minecraft.world.item.Item

object RegisterItems {

    @JvmField
    val EDIT_TOOL: Item = register(
        "edit_tool",
        EditTool()
    )
}
