package net.frozenblock.geometrycraft.datafix.schemas

import com.mojang.datafixers.schemas.Schema
import com.mojang.datafixers.types.templates.TypeTemplate
import net.frozenblock.geometrycraft.util.midString
import net.frozenblock.geometrycraft.util.oldString
import net.frozenblock.geometrycraft.util.string
import net.minecraft.util.datafix.schemas.NamespacedSchema
import java.util.function.Supplier

open class GCV6(versionKey: Int, parent: Schema) : NamespacedSchema(versionKey, parent) {

    override fun registerBlockEntities(schema: Schema): MutableMap<String, Supplier<TypeTemplate>> {
        val map = super.registerBlockEntities(schema)
        map[string("jump_pad")] = map.remove(midString("jump_pad"))
        return map
    }

    override fun registerEntities(schema: Schema): MutableMap<String, Supplier<TypeTemplate>> {
        val map = super.registerEntities(schema)
        map[string("checkpoint")] = map.remove(midString("checkpoint"))
        map[string("orb")] = map.remove(midString("orb"))
        map[string("portal")] = map.remove(midString("portal"))
        return map
    }
}
