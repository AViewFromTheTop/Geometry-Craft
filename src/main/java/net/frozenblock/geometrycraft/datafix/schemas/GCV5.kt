package net.frozenblock.geometrycraft.datafix.schemas

import com.mojang.datafixers.schemas.Schema
import com.mojang.datafixers.types.templates.TypeTemplate
import net.frozenblock.geometrycraft.util.midString
import net.frozenblock.geometrycraft.util.oldString
import net.minecraft.util.datafix.schemas.NamespacedSchema
import java.util.function.Supplier

open class GCV5(versionKey: Int, parent: Schema) : NamespacedSchema(versionKey, parent) {

    override fun registerEntities(schema: Schema): MutableMap<String, Supplier<TypeTemplate>> {
        val map = super.registerEntities(schema)
        map[midString("checkpoint")] = map.remove(oldString("checkpoint"))
        map[midString("orb")] = map.remove(oldString("orb"))
        map[midString("portal")] = map.remove(oldString("portal"))
        return map
    }
}
